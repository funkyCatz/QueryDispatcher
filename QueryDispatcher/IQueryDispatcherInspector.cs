namespace QueryDispatcher
{
    public interface IQueryDispatcherInspector
    {
        void HandleQuery<TResult>(QueryWrapper<TResult> query);
        void HandleResponse<TResult>(QueryWrapper<TResult> query, TResult result);
    }

    public static class QueryDispatcherInspectorLocator
    {
        public static IQueryDispatcherInspector Instance {get; private set;}

        public static void SetInstance(IQueryDispatcherInspector value)
        {
            if (Instance == null)
            {
                Instance = value;
            }
        }
    }
}