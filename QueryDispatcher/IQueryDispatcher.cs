using System;
using System.Threading.Tasks;

namespace QueryDispatcher
{
    public interface IQueryDispatcher
    {
        TResult FindAndExecute<TResult>(IQuery<TResult> query);
        TResult FindAndExecuteWithKey<TResult>(IQuery<TResult> query, object key);
        Task<TResult> FindAndExecuteAsync<TResult>(IQuery<TResult> query);
    }
}