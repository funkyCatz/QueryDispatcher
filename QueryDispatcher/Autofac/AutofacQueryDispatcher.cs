﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Autofac;
using QueryDispatcher.Exceptions;

namespace QueryDispatcher.Autofac
{
    public class AutofacQueryDispatcher : IQueryDispatcher
    {
        private readonly IComponentContext _componentContext;
        private static IQueryDispatcherInspector _queryInspector;

        #region Ctor
        public AutofacQueryDispatcher(IComponentContext componentContext)
        {
            _componentContext = componentContext ?? throw new ArgumentNullException(nameof(componentContext));
            _queryInspector = QueryDispatcherInspectorLocator.Instance;
        }
        #endregion

        private dynamic findHandler<TResult>(IQuery<TResult> query, object key = null)
        {
            if (query == null) throw new ArgumentNullException(nameof(query));

            Type handlerType = typeof(IQueryHandler<,>).MakeGenericType(query.GetType(), typeof(TResult));

            object resolvedInstance;
            var res = key == null
                ? _componentContext.TryResolve(handlerType, out resolvedInstance)
                : _componentContext.TryResolveKeyed(key, handlerType, out resolvedInstance);

            var result = res ? resolvedInstance : null;

            return result;
        }

        private TResult findAndExecute<TResult>(IQuery<TResult> query, dynamic queryHandler)
        {
            if (queryHandler == null)
            {
                throw new QueryHandlerNotFoundException(
                    $"Query handler for query type {query.GetType().FullName} not found");
            }

            var queryWrapper = new QueryWrapper<TResult>(query);

            _queryInspector?.HandleQuery(queryWrapper);

            var result = queryHandler.Execute((dynamic)query);

            if (_queryInspector != null)
            {
                _queryInspector.HandleResponse(queryWrapper, result);
            }

            return result;
        }

        private async Task<TResult> findAndExecuteAsync<TResult>(IQuery<TResult> query, dynamic queryHandler)
        {
            if (queryHandler == null)
            {
                throw new QueryHandlerNotFoundException(
                    $"Query handler for query type {query.GetType().FullName} not found");
            }

            var queryWrapper = new QueryWrapper<TResult>(query);

            _queryInspector?.HandleQuery(queryWrapper);

            var result = await queryHandler.ExecuteAsync((dynamic)query);

            if (_queryInspector != null)
            {
                _queryInspector.HandleResponse(queryWrapper, result);
            }

            return result;
        }

        #region IQueryDispatcher implementation
        public TResult FindAndExecute<TResult>(IQuery<TResult> query)
        {
            return findAndExecute(query, findHandler(query));
        }

        public TResult FindAndExecuteWithKey<TResult>(IQuery<TResult> query, object key)
        {
            return findAndExecute(query, findHandler(query, key));
        }

        public async Task<TResult> FindAndExecuteAsync<TResult>(IQuery<TResult> query)
        {
            return await findAndExecuteAsync(query, findHandler(query));
        }
        #endregion
    }
}
