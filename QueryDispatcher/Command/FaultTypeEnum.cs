﻿namespace QueryDispatcher.Command
{
    public enum FaultTypeEnum
    {
        Required = 0,
        InvalidData = 1,
        InvalidFormat = 2,
        Other = 3
    }
}
