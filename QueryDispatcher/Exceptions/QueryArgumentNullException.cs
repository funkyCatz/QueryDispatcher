﻿using System;

namespace QueryDispatcher.Exceptions
{
    public class QueryArgumentNullException : ArgumentException
    {
    }
}
