﻿using System;
using System.Threading.Tasks;
using QueryDispatcher.Exceptions;

namespace QueryDispatcher.MSDI
{
    public class MSDIQueryDispatcher : IQueryDispatcher
    {
        private readonly IServiceProvider _serviceProvider;
        private static IQueryDispatcherInspector _queryInspector;

        #region Ctor
        public MSDIQueryDispatcher(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider));
            _queryInspector = QueryDispatcherInspectorLocator.Instance;
        }
        #endregion

        private dynamic findHandler<TResult>(IQuery<TResult> query, object key = null)
        {
            if (query == null) throw new ArgumentNullException(nameof(query));

            Type handlerType = typeof(IQueryHandler<,>).MakeGenericType(query.GetType(), typeof(TResult));

            var resolvedInstance =  _serviceProvider.GetService(handlerType);

            return resolvedInstance;
        }

        private TResult findAndExecute<TResult>(IQuery<TResult> query, dynamic queryHandler)
        {
            if (queryHandler == null)
            {
                throw new QueryHandlerNotFoundException(
                    $"Query handler for query type {query.GetType().FullName} not found");
            }

            var queryWrapper = new QueryWrapper<TResult>(query);

            _queryInspector?.HandleQuery(queryWrapper);

            var result = queryHandler.Execute((dynamic)query);

            _queryInspector?.HandleResponse(queryWrapper, result);

            return result;
        }

        private async Task<TResult> findAndExecuteAsync<TResult>(IQuery<TResult> query, dynamic queryHandler)
        {
            if (queryHandler == null)
            {
                throw new QueryHandlerNotFoundException(
                    $"Query handler for query type {query.GetType().FullName} not found");
            }

            var queryWrapper = new QueryWrapper<TResult>(query);

            _queryInspector?.HandleQuery(queryWrapper);

            var result = await queryHandler.ExecuteAsync((dynamic)query);

            if (_queryInspector != null)
            {
                _queryInspector.HandleResponse(queryWrapper, result);
            }

            return result;
        }

        #region IQueryDispatcher implementation
        public TResult FindAndExecute<TResult>(IQuery<TResult> query)
        {
            return findAndExecute(query, findHandler(query));
        }

        /// <summary>
        /// For now this method is similar to FindAndExecute
        /// </summary>
        /// <param name="query">query</param>
        /// <param name="key">redundant param</param>
        /// <typeparam name="TResult"></typeparam>
        /// <returns></returns>
        public TResult FindAndExecuteWithKey<TResult>(IQuery<TResult> query, object key)
        {
            return findAndExecute(query, findHandler(query, key));
        }

        public async Task<TResult> FindAndExecuteAsync<TResult>(IQuery<TResult> query)
        {
            return await findAndExecuteAsync(query, findHandler(query));
        }
        #endregion
    }
}