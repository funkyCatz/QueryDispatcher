using System;
using System.Threading.Tasks;

namespace QueryDispatcher
{
    public interface IQueryHandler<in TQuery, out TResult>
    {
        TResult Execute(TQuery query);
    }

    public interface IAsyncQueryHandler<in TQuery, TResult> : IQueryHandler<TQuery, TResult>
    {
        Task<TResult> ExecuteAsync(TQuery query);
    }
}