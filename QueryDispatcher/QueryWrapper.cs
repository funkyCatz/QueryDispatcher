﻿using System.Diagnostics;

namespace QueryDispatcher
{
    public class QueryWrapper<TResult>
    {
        public QueryWrapper(IQuery<TResult> query)
        {
            Query = query;
            TimeStamp = Stopwatch.GetTimestamp();
        }
        public IQuery<TResult> Query { get; private set; }
        public long TimeStamp { get; private set; }
    }

}

