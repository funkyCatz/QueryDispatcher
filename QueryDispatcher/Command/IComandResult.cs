﻿using System.Collections.Generic;

namespace QueryDispatcher.Command
{
    public interface ICommandResult
    {
        bool Successful { get; }

        IEnumerable<IValidationItem> Errors { get; }
        IEnumerable<IValidationItem> Warnings { get; }
        IEnumerable<string> SuccessSummary { get; }
    }
}
