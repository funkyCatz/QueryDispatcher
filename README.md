# QueryDispatcher
Dispatcher for autofac container and microsoft DI needed to implement
CQRS pattern.

# Usage
Register type in the container :

    var builder = new ContainerBuilder();
    builder.RegisterType<AutofacQueryDispatcher>().As<IQueryDispatcher>();
    
Or:  

    services.AddTransient<IQueryDispatcher, MSDIQueryDispatcher>();

Register commands or queries :

    builder.RegisterType<DummyCommandHandler>().As<IQueryHandler<DummyCommand, ICommandResult>>();

Now you can dispatch registered command :
    
     public class DummyController : Controller
     {
        private readonly IQueryDispatcher _queryDispatcher;

        public DummyController(IQueryDispatcher queryDispatcher)
        {
            _queryDispatcher = queryDispatcher;
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update(DummyCommand commandModel)
        {
            var viewModel = _queryDispatcher.FindAndExecute(commandModel);

            return View("Dummy", viewModel);
        }
     }

# Todo

1) Add tests
2) Separate dispatcher code from autofac (should it be splitted into two different projects?)