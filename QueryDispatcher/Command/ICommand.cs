﻿namespace QueryDispatcher.Command
{
    public interface ICommand : IQuery<ICommandResult>
    {
    }

    public interface ICommand<out TCommandResult> : IQuery<TCommandResult>
        where TCommandResult : ICommandResult
    {
    }
}
