﻿namespace QueryDispatcher.Command
{
    public interface ICommandHandler<in TCommand> : IQueryHandler<TCommand, ICommandResult>
    {
    }

    public interface ICommandHandler<in TCommand, out TCommandResult> : IQueryHandler<TCommand, TCommandResult>
        where TCommandResult : ICommandResult
    {
    }
}
