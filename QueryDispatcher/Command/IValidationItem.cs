﻿using System.Collections.Generic;

namespace QueryDispatcher.Command
{
    public interface IValidationItem
    {
        FaultTypeEnum FaultType { get; }

        List<string> Messages { get; set; }

        string Field { get; set; }

        string Context { get; set; }
    }
}
